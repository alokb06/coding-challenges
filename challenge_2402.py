import math


def get_sum_pair(num):
    half = math.floor(num/2)
    a = get_nearest(half, 0)
    b = str(num - a)

    while b.find('4') > -1:
        t = get_nearest(int(b, 10), a)
        d = int(b, 10) - t
        b = str(a + d)
        a = t

    print(str(a) + ", " + b)


def get_nearest(a1, b):

    a = str(a1)
    if a.find('4') == -1:
        return a

    i = len(a) - a.find('4') - 1
    f = 10**i
    mn = math.floor(a1/f)*f - 1
    mx = math.floor(a1/f)*f + f

    if b == mn:
        return mx
    elif b == mx:
        return mn

    return mx if (a1-mn > mx-a1) else mn



# To run uncomment the below code:

#get_sum_pair(80808080808080808080)